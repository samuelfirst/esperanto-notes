* Nouns end with 'o'
** ex:  Tablo (table)
**     Gitaro (guitar)
**       Seĝo (chair)
**       Lito (bed)
**   koncerto (concert)
**       muro (wall)
**      afiŝo (poster)
**  komputilo (computer)
* Adjectives end with 'a'
** ex:   bela (beautiful)
**    moderna (modern)
**  malgranda (small)
**       pura (clean)
**    stranga (strange)
**     granda (big)
* Tenses
** Present Tense ends with 'as'
*** ex: Sidas (sitting)
***     kuŝas (lies)
***     estas (is)
* Negation is prefaced with 'ne'
** ex:   ne pura (not clean)
**     ne granda (not big)
**    ne moderna (not modern)
** Alternatively, if a word begins with mal, it means the opposite
*** ex:  malpura (dirty)
***    malgranda (small)
***   malmoderna (old)
* Ĝi ~= it
** ex: Ĝi estas bela. (It is beautiful.)
* A word ending with 'ej[a/o]' means a place intended for something
** ex: librejo (book store)
**     lernejo (place for learning)
**      loĝejo (house/dwelling)
* To make a word plural, end with 'j'
** ex.        tago (one day)    -->          tagoj (several days)
**     granda domo (big house)  -->  grandaj domoj (big houses)
* A word with '-ul-', then a suffix means a person w/ a characteristic
** ex. junulo (young person)
**     dikulo (fat person)
**  milionulo (millionaire)
* A word with '-ist-', then a suffix means a professional
** ex. verkisto (writer)
**      artisto (artist)
**    kruacisto (doctor)
* A word with '-uj-', then a suffix means something that contains that thing
** ex. cigarujo (cigar box)
**       monujo (wallet)
** When used with the name of a people, it means the country of that people
*** ex. Francujo (France)
***     Somalujo (Somalia)
* A word with '-er-', then a suffix means a small part of that thing
** ex. sablero (grain of sand)
**        neĝo (snowflake)
**     fajrero (spark)
